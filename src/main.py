from trainerbase.main import run

from gui import run_menu
from scripts import regen_script_engine, update_dll_address_script_engine


def on_initialized():
    regen_script_engine.start()
    update_dll_address_script_engine.start()


if __name__ == "__main__":
    run(run_menu, on_initialized)
