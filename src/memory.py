from typing import ClassVar, override

from trainerbase.memory import Address, get_module_address


class CODGameX86Address(Address):
    dll_base_address: ClassVar[int | None] = None

    @override
    def __init__(self, base_address: int, offsets: list[int] | None = None, add: int = 0):
        self.dll_relative_base_address = base_address
        super().__init__(base_address, offsets, add)

    @classmethod
    def update_dll_base_address(cls):
        cls.dll_base_address = get_module_address("uo_gamex86.dll", retry_limit=32)

    @override
    def resolve(self) -> int:
        if self.__class__.dll_base_address is None:
            self.update_dll_base_address()

        assert self.__class__.dll_base_address

        self.base_address = self.__class__.dll_base_address + self.dll_relative_base_address

        return super().resolve()


player_coords_address = CODGameX86Address(0x2E7254)
hp_address = CODGameX86Address(0x154BA0)
