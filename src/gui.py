import dearpygui.dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI, TextUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.scripts import ScriptUI
from trainerbase.gui.teleport import TeleportUI

from injections import infinite_ammo, rapid_fire
from objects import hp
from scripts import regenerate_hp
from teleport import tp


@simple_trainerbase_menu("Call of Duty®", 620, 280)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                TextUI("Shooting"),
                CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F1"),
                CodeInjectionUI(rapid_fire, "Rapid Fire", "F2"),
                SeparatorUI(),
                TextUI("Health"),
                GameObjectUI(hp, "HP", "F3"),
                ScriptUI(regenerate_hp, "Regenerate HP", "F4"),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp))
