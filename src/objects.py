from typing import override

from trainerbase.gameobject import AbstractReadableObject, GameFloat, GameInt

from memory import hp_address, player_coords_address


class HPStub(AbstractReadableObject[int]):
    @property
    @override
    def value(self) -> int:
        return 200


hp = GameInt(hp_address)
max_hp_stub = HPStub()

player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address + 0x4)
player_z = GameFloat(player_coords_address + 0x8)
